use std::env;
use std::process::Command;


const HELP_MENU: &str = "'editrs [command] [options] [file]'\n\nA small video editing tool based on ffmpeg, written in rust\nWarning! This tool will edit a video file IN PLACE, so if you're worried about mistakes, make a backup first";
const VERSION: &'static str = env!("CARGO_PKG_VERSION");
const AUTHORS: &'static str = env!("CARGO_PKG_AUTHORS");


fn get_banner() -> std::string::String {
    format!(
        "{} Version {}
Convert granite to html\n",
        AUTHORS, VERSION,
    )
}


fn usage() {
    println!("{}", get_banner());
    println!("{}", HELP_MENU);
}


fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let args = &args[1..];
    match &args[..] {
        [] => usage(),
        [cmd] => match cmd.as_str() {
            "-v" | "--version" => println!("Version: {}", VERSION),
            "-h" | "--help" => usage(),
            _ => println!("{}", HELP_MENU),
        },
        [cmd, opt, file] => match cmd.as_str() {
            "trim" => handle_trim(opt, file),
            "join" => handle_join(opt, file),
            "text" => handle_text(opt, file),
        	_ => println!("{}", HELP_MENU),
        },
        _ => usage(),
    }

	Ok(())
}


fn handle_trim(opt: &str, file: &str) {
	println!("trimming: {} -> {}", opt, file);

	let split: Vec<&str> = opt.split_whitespace().collect();
	let start = split[0];
	let end = split[1];

	// let i = format!("-i {} ", file);
	// let ss = format!("-ss {} ", start);
	// let t = format!("-t {} ", end);

	// let args = &[i, ss, t, "-async 1 ".to_string(), file.to_string()];
	// println!("{}", args.concat());

	let output = Command::new("ffmpeg")
		.arg("-i")
		.arg(file)
		.arg("-ss")
		.arg(start)
		.arg("-t")
		.arg(end)
		.arg("-async 1")
		.arg("1")
		.arg("temp-adsfadfadfadfadsfasdf.mp4")
		.output().unwrap();
	if !output.status.success() {
	    let s = String::from_utf8_lossy(&output.stderr);
	    print!("command failed; stderr was:\n{}", s);
	}
}


fn handle_join(opt: &str, file: &str) {
	println!("joined: {}, {}", opt, file);
}


fn handle_text(opt: &str, file: &str) {
	println!("added text: {}, {}", opt, file);
}
